//export let baseUrl = baseUrl

import {baseUrl} from "@public/config";

export let addUser = '/api/Systems/add'//注册
export let DeleteUser = '/api/Systems/Delete'//删除用户
export let Login   = '/api/Systems/Login'//登录
export let GetUserList =  '/api/Systems/GetUserList'//获取用户列表
export  let UpdatePwd='/api/Systems/UpdatePwd'
export let Update ='/api/Systems/Update' //更新用户信息
export let GetLogList = '/api/Systems/GetLogList'

export let editList = '/api/Software/Update'//修改APP
export let getList = '/api/Software/GetList'//获取APP列表
export let addList = '/api/Software/Add'//添加APP列表
export let deleteList = '/api/Software/Delete'//删除APP列表
export let getMaxCode= '/api/Software/GetMaxCode'
export let addVer = '/api/Dpgrade/Add'//添加版本
export let getVerList ='/api/Dpgrade/GetList'//获取版本列表
export let deleteVer = 'api/Dpgrade/Delete'//删除版本
export let SetRecommend = '/api/Dpgrade/SetRecommend'//设置推荐
export let SetRelease = '/api/Dpgrade/SetRelease'//设置发布
export let Upload  =baseUrl+'/api/Upgrade/Upload'//上传WGT
export let UploadAPK  =baseUrl+'/api/Upgrade/UpgradeUpload'//上传APK

export let UpdateVer = '/api/Dpgrade/Update'
export let GetEntity = '/api/Dpgrade/GetEntity'



