import Vue from 'vue'
import Vuex from 'vuex'
import {Login} from "../api/baseService";

Vue.use(Vuex)

const state = {
     userInfo:{
         accress: "",
         company: "",
         createTime: "",
         createUserId: "",
         email: "",
         id: 0,
         isDelete: 0,
         modifyTime: "",
         name: "",
         password: "",
         phone: "",
         post: ""
     },
     editUser:false
}

const mutations = {
    Get_UserInfo(state,userInfo){
        sessionStorage.setItem(`userInfo`, JSON.stringify(userInfo))
        state.userInfo = userInfo
    },
    EDIT_USER(state,editUser){
        state.editUser = editUser
    }
}

const actions = {}

export default new Vuex.Store({
    state,
    actions,
    mutations,
})
