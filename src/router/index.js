import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import register from "@/views/register";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home.vue')
  },
 /* {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue')
  },*/
  {
    path: '/main',

    component: () => import( '@/views/main.vue'),
    children:[
      {
        path:'/',
        component:()=>import('@/components/MainTable')
      },
      {
        path:'/manage',
        component:()=>import('@/components/SystemManage')
      },
      {
        path:'/log',
            component:()=>import('@/components/LoginLog')
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.NODE_ENV === "development" ? '/' : '/',
  routes
})
router.beforeEach((to, from, next)=>{
  let token = localStorage.getItem('token')
  if(token || to.path=='/' || to.path=='register') {
    next()
  }else {
    next('/')
  }
})
export default router
