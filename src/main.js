import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import ElementUI from 'element-ui';
import store from "@/Store/store";
//import axios from 'axios';
import 'element-ui/lib/theme-chalk/index.css';
import $axios from "@/unilt/request";
import {baseUrl} from "../public/config";
/*import Clipboard from 'v-clipboard'
Vue.use(Clipboard)*/
$axios.defaults.baseURL = baseUrl
Vue.prototype.$axios = $axios
//Vue.use($axios)
Vue.config.productionTip = false
Vue.use(ElementUI)
//Vue.use(axios)
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
