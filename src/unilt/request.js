
import axios from 'axios'
//axios.defaults.baseURL = 'http://124.70.1.242:9997'

const $axios = axios.create({
    timeout: 5000
})

// request 拦截器
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
$axios.interceptors.request.use(config => {
   // console.log('123')
    config.headers['Content-Type'] = 'application/json;charset=utf-8';
    //console.log(localStorage.getItem("token"))
   if(localStorage.token){
       //console.log(localStorage.token)
       //config.headers['token'] = localStorage.getItem("token");  // 设置请求头
       config.headers.Authorization = 'Bearer '+localStorage.token
    }
    return config
}, error => {
    return Promise.reject(error)
});
// response 拦截器
// 可以在接口响应后统一处理结果
$axios.interceptors.response.use(
    response => {
        let res = response.data;
        // 如果是返回的文件
        if (response.config.responseType === 'blob') {
            return res
        }
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        /*if(res.code == "200"){
            localStorage.clear()
            alert(res.Message)
            this.$router.push('/')
        }*/
        return res;
    },
    error => {
        console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)

export default $axios

