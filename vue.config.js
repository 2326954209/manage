

const path = require("path");
//const baseUrl = require('./public/config')
module.exports = {
    chainWebpack: config => {
        config.resolve.alias
            .set("@public", path.resolve(__dirname, "./public"))
            .set("@", path.resolve(__dirname, "./src"))
            .set("@assets", path.resolve(__dirname, "./src/assets"))
            .set("@components", path.resolve(__dirname, "./src/components"))
            .set("@views", path.resolve(__dirname, "./src/views"))
            .set("@service", path.resolve(__dirname, "./src/service"))

    },
    publicPath: process.env.NODE_ENV === "development" ? './' : './',

    devServer: {
        //port: 8080,
        proxy: {                 //设置代理

            '/api': {              //设置拦截器  拦截器格式   斜杠+拦截器名字
                /*target: 'http://localhost:5050/',*/     //代理的目标地址

                target: require('./public/config'),
                changeOrigin: true,              //是否设置同源
                pathRewrite: {                   //路径重写
                    '/api': '',//重写,                   //选择忽略拦截器里面的单词
                }
            }
        }
    },
    /*build:{
        assetsPublicPath:'./'
    },*/
    pwa: {
        iconPaths: {
            favicon32: 'favicon.ico',
            favicon16: 'favicon.ico',
            appleTouchIcon: 'favicon.ico',
            maskIcon: 'favicon.ico',
            msTileImage: 'favicon.ico'
        }
    },


}
