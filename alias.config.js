const resolve = dir => require('path').join(__dirname, dir)

module.exports = {
    resolve: {
        alias: {
            '@': resolve('src'),
            '@components': resolve('src/components'),
            '@views':resolve('src/views'),
            '@services':resolve('src/services'),
            '@assets':resolve('src/assets'),
            '@common':resolve('src/common'),
        },
    },
}
/*
*
*  .set("@assets", path.resolve(__dirname, "./src/assets"))
      .set("@components", path.resolve(__dirname, "./src/components"))
      .set("@views", path.resolve(__dirname, "./src/views"))
      .set("@services", path.resolve(__dirname, "./src/services"))
            .set("@common", path.resolve(__dirname, "./src/common"));

*
* */
